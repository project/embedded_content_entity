<?php

declare(strict_types=1);

namespace Drupal\embedded_content_entity\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\embedded_content\EmbeddedContentPluginManager;
use Drupal\entity_browser\Entity\EntityBrowser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Embedded Content: Entity settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  const ALL = 'all';

  const FORM_WRAPPER = 'embedded-content-entity-settings-form-wrapper';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * The embedded content plugin manager.
   *
   * @var \Drupal\embedded_content\EmbeddedContentPluginManager
   */
  protected EmbeddedContentPluginManager $embeddedContentPluginManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $displayRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('entity_type.manager'),
          $container->get('entity_type.bundle.info'),
          $container->get('plugin.manager.embedded_content'),
          $container->get('entity_display.repository'),
          $container->get('module_handler')
      );
  }

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\embedded_content\EmbeddedContentPluginManager $embedded_content_plugin_manager
   *   The embedded content plugin manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EmbeddedContentPluginManager $embedded_content_plugin_manager, EntityDisplayRepositoryInterface $display_repository, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $entity_type_bundle_info;
    $this->embeddedContentPluginManager = $embedded_content_plugin_manager;
    $this->displayRepository = $display_repository;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'embedded_content_entity_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['embedded_content_entity.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $settings_form = [];
    $settings = [];
    $entity_types = $this->config('embedded_content_entity.settings')->get('entity_types') ?? [];
    foreach ($entity_types as $entity_type_info) {
      $settings[$entity_type_info['entity_type_id']] = $entity_type_info;
    }
    $this->doBuildForm($settings_form, $form_state, $settings);
    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ] + $settings_form;

    return $form;
  }

  /**
   * Build the settings element.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param array $entity_browser_options
   *   The entity browser options.
   * @param array $view_modes
   *   The view modes.
   * @param array $settings
   *   The settings.
   * @param string|null $bundle
   *   The bundle.
   *
   * @return array
   *   The settings element.
   */
  public static function settingsElement(EntityTypeInterface $entity_type, array $entity_browser_options, array $view_modes, array $settings, ?string $bundle = NULL) {
    if ($bundle) {
      $settings = NestedArray::getValue(
            $settings, [
              $entity_type->id(),
              'bundles',
              $bundle,
            ]
        );
    }
    else {
      $settings = NestedArray::getValue(
            $settings, [
              $entity_type->id(),
            ]
        );
    }
    $element = [
      'inline' => [
        '#type' => 'checkbox',
        '#title' => new TranslatableMarkup('Inline'),
        '#default_value' => $settings['inline'] ?? FALSE,
        '#description' => new TranslatableMarkup('Will embed the entity inline in the text.'),
      ],
    ];
    if (!empty($entity_browser_options)) {
      $element['entity_browser'] = [
        '#type' => 'select',
        '#title' => new TranslatableMarkup('Entity browser'),
        '#empty_option' => new TranslatableMarkup('- None -'),
        '#description' => new TranslatableMarkup('If none selected, the entity autocomplete widget will be used.'),
        '#options' => $entity_browser_options,
        '#default_value' => $settings['entity_browser'] ?? FALSE,
        '#ajax' => [
          'callback' => [self::class, 'updateForm'],
          'wrapper' => self::FORM_WRAPPER,
        ],
      ];
      if ($settings['entity_browser'] ?? FALSE) {
        $element['preview_view_mode'] = [
          '#type' => 'select',
          '#title' => new TranslatableMarkup('Preview view mode'),
          '#description' => new TranslatableMarkup('The selected view mode will be used for previewing the entity.'),
          '#options' => $view_modes,
          '#default_value' => $settings['preview_view_mode'] ?? '',
        ];
      }
    }
    if (!empty($view_modes)) {
      $element['view_modes'] = [
        '#type' => 'checkboxes',
        '#title' => new TranslatableMarkup('View modes'),
        '#description' => new TranslatableMarkup('If one is selected, the entity will be rendered using the selected view mode. If more view modes are selected, the entity will be rendered using the selected view mode selected in ckeditor. If none selected, the entity will be rendered using the default view mode.'),
        '#options' => $view_modes,
        '#default_value' => $settings['view_modes'] ?? [],
      ];
    }

    return $element;
  }

  /**
   * Updates the settings element on ajax.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The settings form.
   */
  public static function updateForm(array &$form, FormStateInterface $form_state): array {
    return $form['settings'];
  }

  /**
   * Builds the settings form.
   *
   * @param array $settings_form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $settings
   *   The settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function doBuildForm(array &$settings_form, FormStateInterface $form_state, array $settings) {
    $settings = $form_state->getValue('settings') ?? $settings;
    $settings_form['#prefix'] = '<div id="' . self::FORM_WRAPPER . '">';
    $settings_form['#suffix'] = '</div>';
    $entity_types = $this->entityTypeManager->getDefinitions();

    $entity_browser_options = FALSE;
    if ($this->moduleHandler->moduleExists('entity_browser')) {
      $entity_browser_options = array_map(
            function (EntityBrowser $entity_browser) {
                return $entity_browser->label();
            }, $this->entityTypeManager->getStorage('entity_browser')
              ->loadMultiple()
        );
    }
    foreach ($entity_types as $entity_type) {
      if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
        continue;
      }
      $view_modes = $this->displayRepository->getViewModeOptions($entity_type->id());
      if ($entity_type->hasKey('bundle')) {
        $settings_form[$entity_type->id()] = [
          '#type' => 'details',
          '#title' => $entity_type->getLabel(),
          '#open' => $settings[$entity_type->id()]['enable'] ?? NULL,
          'enable' => [
            '#type' => 'checkbox',
            '#title' => new TranslatableMarkup('Enable'),
            '#default_value' => $settings[$entity_type->id()]['enable'] ?? NULL,
            '#description' => new TranslatableMarkup('If enabled, the entity will be embeddable in formatted text.'),
            '#ajax' => [
              'callback' => [self::class, 'updateForm'],
              'wrapper' => self::FORM_WRAPPER,
            ],
          ],
        ];

        if ($settings[$entity_type->id()]['enable'] ?? FALSE) {
          $settings_form[$entity_type->id()]['per_bundle'] = [
            '#type' => 'checkbox',
            '#title' => new TranslatableMarkup('Per bundle'),
            '#default_value' => $settings[$entity_type->id()]['per_bundle'] ?? FALSE,
            '#description' => new TranslatableMarkup('If enabled, specific bundles of entity will be embeddable in formatted text using embedded content plugins.'),
            '#ajax' => [
              'callback' => [self::class, 'updateForm'],
              'wrapper' => self::FORM_WRAPPER,
            ],
          ];
          if ($settings[$entity_type->id()]['per_bundle'] ?? FALSE) {
            $bundles = $this->bundleInfo->getBundleInfo($entity_type->id());
            foreach ($bundles as $bundle => $info) {
              $view_modes_per_bundle = $this->displayRepository->getViewModeOptionsByBundle($entity_type->id(), $bundle);
              $settings_form[$entity_type->id()]['bundles'][$bundle] = [
                '#type' => 'details',
                '#open' => $settings[$entity_type->id()]['bundles'][$bundle]['enable'] ?? NULL,
                '#title' => $info['label'],
                'enable' => [
                  '#type' => 'checkbox',
                  '#title' => new TranslatableMarkup('Enable'),
                  '#default_value' => $settings[$entity_type->id()]['bundles'][$bundle]['enable'] ?? NULL,
                  '#ajax' => [
                    'callback' => [self::class, 'updateForm'],
                    'wrapper' => self::FORM_WRAPPER,
                  ],
                ],
              ] + ($settings[$entity_type->id()]['bundles'][$bundle]['enable'] ?? FALSE ? $this->settingsElement($entity_type, $entity_browser_options, $view_modes_per_bundle, $settings, $bundle) : []);
            }
          }
          else {
            $settings_form[$entity_type->id()] += $this->settingsElement($entity_type, $entity_browser_options, $view_modes, $settings);
          }
        }
      }
      else {
        $settings_form[$entity_type->id()] = [
          '#type' => 'details',
          '#title' => $entity_type->getLabel(),
          '#open' => $settings[$entity_type->id()]['enable'] ?? FALSE,
          'enable' => [
            '#type' => 'checkbox',
            '#title' => new TranslatableMarkup('Enable'),
            '#default_value' => $settings[$entity_type->id()]['enable'] ?? FALSE,
            '#ajax' => [
              'callback' => [self::class, 'updateForm'],
              'wrapper' => self::FORM_WRAPPER,
            ],
          ],
        ] + ($settings[$entity_type->id()]['enable'] ?? FALSE ? $this->settingsElement($entity_type, $entity_browser_options, $view_modes, $settings) : []);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $settings = $form_state->getValue('settings');
    $config = $this->config('embedded_content_entity.settings');
    $entity_types = [];
    foreach ($settings as $entity_type_id => $setting) {
      $entity_types[] = [
        'entity_type_id' => $entity_type_id,
      ] + $setting;
    }
    $config->set('entity_types', $entity_types);
    $config->save();
    $this->embeddedContentPluginManager->clearCachedDefinitions();
    parent::submitForm($form, $form_state);
  }

}
