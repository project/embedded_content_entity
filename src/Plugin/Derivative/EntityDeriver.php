<?php

namespace Drupal\embedded_content_entity\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\embedded_content_entity\Plugin\EmbeddedContent\Entity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides embedded content plugins for entities.
 *
 * @see \Drupal\embedded_content_entity\Plugin\EmbeddedContent\Entity
 */
class EntityDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\Drupal\embedded_content_entity\Plugin\Derivative\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a new EntityDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->bundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
          $container->get('entity_type.manager'),
          $container->get('config.factory'),
          $container->get('entity_type.bundle.info')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $settings = $this->configFactory->get('embedded_content_entity.settings')
      ->get('entity_types');

    foreach ($settings as $entity_type_settings) {
      $entity_type_id = $entity_type_settings['entity_type_id'];
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($entity_type_settings['enable'] !== TRUE) {
        continue;
      }
      if (($entity_type_settings['per_bundle'] ?? FALSE)) {
        foreach ($entity_type_settings['bundles'] as $bundle_id => $bundle_settings) {
          $bundle_info = $this->bundleInfo->getBundleInfo($entity_type_id)[$bundle_id];
          if ($bundle_settings['enable'] === FALSE) {
            continue;
          }
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['id'] = $entity_type_id . '__' . $bundle_id;
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['label'] = $entity_type->getLabel() . ': ' . $bundle_info['label'];
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['bundle_id'] = $bundle_id;
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['entity_type_id'] = $entity_type_id;
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['view_modes'] = $bundle_settings['view_modes'];
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['inline'] = $bundle_settings['inline'];
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['entity_browser'] = $bundle_settings['entity_browser'];
          $this->derivatives[$entity_type_id . '__' . $bundle_id]['preview_view_mode'] = $bundle_settings['preview_view_mode'] ?? 'default';
        }
      }
      else {
        $this->derivatives[$entity_type_id]['id'] = $entity_type_id . '__' . $bundle_id;
        $this->derivatives[$entity_type_id]['label'] = $entity_type->getLabel();
        $this->derivatives[$entity_type_id]['entity_type_id'] = $entity_type_id;
        $this->derivatives[$entity_type_id]['view_modes'] = $entity_type_settings['view_modes'];
        $this->derivatives[$entity_type_id]['inline'] = $entity_type_settings['inline'];
        $this->derivatives[$entity_type_id]['entity_browser'] = $entity_type_settings['entity_browser'];
        $this->derivatives[$entity_type_id]['preview_view_mode'] = $entity_type_settings['preview_view_mode'] ?? 'default';
      }
    }
    foreach (array_keys($this->derivatives) as $key) {
      $this->derivatives[$key]['class'] = Entity::class;
      $this->derivatives[$key]['config_dependencies'] = [
        'config' => [
          'embedded_content_entity.settings',
        ],
        'module' => [
          'embedded_content_entity',
        ],
      ];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
