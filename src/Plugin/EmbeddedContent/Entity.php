<?php

namespace Drupal\embedded_content_entity\Plugin\EmbeddedContent;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\embedded_content\EmbeddedContentInterface;
use Drupal\embedded_content\EmbeddedContentPluginBase;
use Drupal\embedded_content_entity\EntityBrowserFormTrait;
use Drupal\entity_browser\Element\EntityBrowserElement;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin iframes.
 *
 * @EmbeddedContent(
 *   id = "entity",
 *   label = @Translation("Entity"),
 *   description = @Translation("Allows inserting entities."),
 *   deriver =
 *   "\Drupal\embedded_content_entity\Plugin\Derivative\EntityDeriver",
 * )
 */
class Entity extends EmbeddedContentPluginBase implements EmbeddedContentInterface, ContainerFactoryPluginInterface {

  use EntityBrowserFormTrait;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|null
   */
  protected ?ContentEntityInterface $entity = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $displayRepository;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected EntityRepository $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('entity_type.manager'),
          $container->get('config.factory'),
          $container->get('entity_display.repository'),
          $container->get('entity.repository')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, EntityDisplayRepositoryInterface $displayRepository, EntityRepository $entity_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->displayRepository = $displayRepository;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Get the entity for this plugin.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  protected function getEntity(): ContentEntityInterface {
    if (!$this->entity) {
      $this->entity = $this->entityRepository->loadEntityByUuid($this->pluginDefinition['entity_type_id'], $this->configuration['entity_uuid']);
    }
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
        [
          'entity_uuid' => NULL,
          'view_mode' => NULL,
        ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $view_mode = $this->configuration['view_mode'];
    $view_builder = $this->entityTypeManager->getViewBuilder($this->pluginDefinition['entity_type_id']);
    return $view_builder->view($this->getEntity(), $view_mode);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted()) {
      $values = $form_state->getValues();
      if (is_scalar($values['entity'] ?? NULL) && is_empty($values['entity'])) {
        $form_state->setErrorByName('entity', $this->t('You must select an entity.'));

      }
      if (!($values['entity']['default_value'] ?? NULL)
            && !($values['entity']['browser']['entities'] ?? NULL)
        ) {
        $form_state->setError($form['entity']['browser'], $this->t('You must select an entity.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    [$entity_type_id, $bundle] = explode('__', $this->getDerivativeId());
    $view_modes = array_filter($this->pluginDefinition['view_modes']);
    $preview_view_mode = $this->pluginDefinition['preview_view_mode'] ?? 'default';
    $entity_browser_id = $this->pluginDefinition['entity_browser'];

    $entity = $this->configuration['entity_uuid'] ? $this->entityRepository->loadEntityByUuid($entity_type_id, $this->configuration['entity_uuid']) : NULL;

    if ($entity_browser_id && ($entity_browser = $this->entityTypeManager->getStorage('entity_browser')->load($entity_browser_id))
      ) {
      // We need a wrapping container for AJAX operations.
      $element = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'entity-browser-wrapper',
        ],
        // We need to add the default value here, because if the entity browser
        // was never opened, the value will not be set.
        'default_value' => [
          '#type' => 'hidden',
          '#title' => $this->t('Default value'),
          '#default_value' => $entity ? implode(
            ':', [
              $entity->getEntityTypeId(),
              $entity->id(),
            ]
          ) : '',
        ],
      ];
      if (!in_array($entity_browser->display, ['modal', 'iframe'])) {
        $element['browser'] = [
          '#type' => 'markup',
          '#markup' => new TranslatableMarkup('Only modal entity browsers are supported'),
        ];
      }
      else {
        if ($entity) {
          $entity_browser->display_configuration['link_text'] = new TranslatableMarkup('Select other @label', ['@label' => $form['#button']->getSingularLabel()]);
        }
        $element['browser'] = [
          '#type' => 'entity_browser',
          '#entity' => $entity,
          '#entity_browser' => $entity_browser,
          '#process' => [
          [self::class, 'processEntityBrowser'],
          ],
          '#cardinality' => 1,
          '#selection_mode' => EntityBrowserElement::SELECTION_MODE_PREPEND,
          '#wrapper_id' => &$element['#attributes']['id'],
          '#weight' => 99,
          '#button' => $form['#button'],
        ];
      }
      $element['selected'] = [
        '#type' => 'container',
        '#process' => [
        [self::class, 'processEntityBrowserSelected'],
        ],
        '#view_mode' => $preview_view_mode,
        '#form_mode' => 'default',
        '#wrapper_id' => &$element['#attributes']['id'],
        '#default_value' => $entity ? implode(
            ':', [
              $entity->getEntityTypeId(),
              $entity->id(),
            ]
        ) : NULL,
      ];
      $form['entity'] = $element;
    }
    else {
      $form['entity'] = [
        '#title' => $this->pluginDefinition['label'],
        '#type' => 'entity_autocomplete',
        '#required' => TRUE,
        '#target_type' => $entity_type_id,
        '#selection_handler' => 'default:' . $entity_type_id,
        '#selection_settings' => [
          'view_modes' => $view_modes,
        ] + ($bundle ? ['target_bundles' => [$bundle]] : []),
        '#cardinality' => 1,
        '#default_value' => $entity,
      ];
    }

    if (count($view_modes) == 0) {
      $view_modes = $bundle ? $this->displayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle) : $this->displayRepository->getViewModeOptions($entity_type_id);
    }
    if (count($view_modes) == 1) {
      $form['view_mode'] = [
        '#type' => 'value',
        '#value' => array_key_first($view_modes),
      ];
    }
    else {
      $form['view_mode'] = [
        '#required' => TRUE,
        '#type' => 'select',
        '#title' => $this->t('View mode'),
        '#empty_option' => $this->t('- Select a view mode -'),
        '#default_value' => $this->configuration['view_mode'],
        '#options' => $view_modes,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function isInline(): bool {
    return (bool) $this->pluginDefinition['inline'];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array &$values, array $form, FormStateInterface $form_state) {
    if (is_scalar($values['entity']) && $values['entity'] !== '') {
      [$entity_type_id] = explode('__', $this->getDerivativeId());
      $entity = $this->entityTypeManager->getStorage($entity_type_id)
        ->load($values['entity']);
    }
    else {
      $entity = $values['entity']['browser']['entities'][0] ?? NULL;
      if (!$entity && ($id = $values['entity']['default_value'])) {
        [$entity_type_id, $entity_id] = explode(':', $id);
        $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
      }
    }
    if ($entity) {
      unset($values['entity']);
      $values['entity_uuid'] = $entity->uuid();
    }
  }

}
