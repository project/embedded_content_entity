<?php

namespace Drupal\embedded_content_entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_browser\Element\EntityBrowserElement;

/**
 * Provides helpers for adding an entity browser element to a form.
 *
 * Based on https://www.drupal.org/project/helper.
 */
trait EntityBrowserFormTrait {

  /**
   * Loads entity based on an ID in the format entity_type:entity_id.
   *
   * @param string $id
   *   An ID.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A loaded entity.
   */
  public static function loadEntityBrowserEntity($id) {
    $entities = static::loadEntityBrowserEntitiesByIds($id);
    return reset($entities);
  }

  /**
   * Loads entities based on an ID in the format entity_type:entity_id.
   *
   * @param array|string $ids
   *   An array of IDs.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of loaded entities, keyed by an ID.
   */
  public static function loadEntityBrowserEntitiesByIds($ids) {
    if (!is_array($ids)) {
      $ids = explode(' ', $ids);
    }
    $ids = array_filter($ids);

    $storage = [];
    $entities = [];
    foreach ($ids as $id) {
      [$entity_type_id, $entity_id] = explode(':', $id, 2);
      if (!isset($storage[$entity_type_id])) {
        $storage[$entity_type_id] = \Drupal::entityTypeManager()
          ->getStorage($entity_type_id);
      }
      $entities[$entity_type_id . ':' . $entity_id] = $storage[$entity_type_id]->load($entity_id);
    }
    return array_filter($entities);
  }

  /**
   * Gets the entity browser form value.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array|string $parents
   *   The parents of the containing form element.
   *
   * @return array
   *   The entity browser value.
   */
  public static function getEntityBrowserValue(FormStateInterface $form_state, $parents) {
    $parents = is_array($parents) ? $parents : [$parents];
    return $form_state->getValue(
          array_merge(
              $parents, [
                'browser',
                'entity_ids',
              ]
          )
      );
  }

  /**
   * Render API callback: Processes the entity browser element.
   */
  public static function processEntityBrowser(&$element, FormStateInterface $form_state, &$complete_form) {
    if (!is_array($element['#default_value'])) {
      $element['#default_value'] = !empty($element['#default_value']) ? static::loadEntityBrowserEntitiesByIds($element['#default_value']) : [];
    }
    if (!empty($element['#value']['entities'] ?? [])) {
      /**
* @var \Drupal\entity_browser\Entity\EntityBrowser $entity_browser
*/
      $entity_browser = $element['#entity_browser'];
      $display = $entity_browser->getDisplay();
      $display->configuration['link_text'] = new TranslatableMarkup('Select other @label', ['@label' => $element['#button']->getSingularLabel()]);
      $entity_browser->display_configuration['auto_open'] = FALSE;
    }
    $element = EntityBrowserElement::processEntityBrowser($element, $form_state, $complete_form);
    $element['entity_ids']['#ajax'] = [
      'callback' => [self::class, 'updateEntityBrowserSelected'],
      'wrapper' => $element['#wrapper_id'],
      'event' => 'entity_browser_value_updated',
    ];
    $element['entity_ids']['#default_value'] = implode(' ', array_keys($element['#default_value']));

    if ($element['entity_ids']['#default_value']) {
      $element['#access'] = FALSE;
    }
    return $element;
  }

  /**
   * Render API callback: Processes the table element.
   */
  public static function processEntityBrowserSelected(&$element, FormStateInterface $form_state, &$complete_form) {
    // Go down one level to the container, then add the browser and entity IDs
    // field to find the right form state value.
    $parents = $element['#array_parents'];
    array_pop($parents);
    $entity_ids = $form_state->getValue(
          array_merge(
              $parents, [
                'browser',
                'entity_ids',
              ]
          ), ''
      ) ?: $element['#default_value'] ?? '';

    $entities = empty($entity_ids) ? [] : self::loadEntityBrowserEntitiesByIds($entity_ids);
    $entity_type_manager = \Drupal::entityTypeManager();

    foreach ($entities as $id => $entity) {
      $entity_type_id = $entity->getEntityTypeId();
      if ($entity_type_manager->hasHandler($entity_type_id, 'view_builder')) {
        $preview = $entity_type_manager->getViewBuilder($entity_type_id)
          ->view($entity, $element['#view_mode']);
      }
      else {
        $preview = ['#markup' => $entity->label()];
      }

      $element[$id] = [
        '#attributes' => [
          'data-entity-id' => $id,
        ],
        'item' => $preview,
      ];
    }
    return $element;
  }

  /**
   * AJAX callback: Re-renders the Entity Browser button/table.
   */
  public static function updateEntityBrowserSelected(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if (isset($trigger['#op']) && $trigger['#op'] === 'remove') {
      $parents = array_slice($trigger['#array_parents'], 0, -4);
      $selection = NestedArray::getValue($form, $parents);
      $id = str_replace('entity_browser_remove_', '', $trigger['#name']);
      unset($selection['selected'][$id]);
      $value = explode(' ', $selection['browser']['entity_ids']['#value']);
      $selection['browser']['entity_ids']['#value'] = array_diff($value, [$id]);
    }
    else {
      $parents = array_slice($trigger['#array_parents'], 0, -2);
      $selection = NestedArray::getValue($form, $parents);
    }
    return $selection;
  }

}
